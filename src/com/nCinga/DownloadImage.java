package com.nCinga;

import java.awt.*;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class DownloadImage extends Thread {
    String imageURL;
    Image image;

    public DownloadImage(String url) {
        this.imageURL = url;
    }

    @Override
    public void run() {
        try {
            URL url = new URL(imageURL);
            String imagePath = url.getFile();
            String imageName = imagePath.substring(imagePath.lastIndexOf("/")+1);
            System.out.println("Downloading "+imageName+" ...");
            InputStream inputStream = url.openStream();
            File image= new File("Images/"+imageName);
            OutputStream outputStream = new FileOutputStream(image);

            byte[] b = new byte[2048];
            int length;

            while ((length = inputStream.read(b)) != -1) {
                outputStream.write(b, 0, length);
            }
            inputStream.close();
            outputStream.close();
            System.out.println(imageName+" downloaded");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}