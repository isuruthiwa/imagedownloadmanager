package com.nCinga;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {

    public static void main(String[] args) {
        ThreadPoolExecutor executor= (ThreadPoolExecutor) Executors.newFixedThreadPool(5);

        DownloadImage image1= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/airplane.png");
        DownloadImage image2= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png");
        DownloadImage image3= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/baboon.png");
        DownloadImage image4= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/boat.png");
        DownloadImage image5= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/cat.png");
        DownloadImage image6= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/fruits.png");
        DownloadImage image7= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/frymire.png");
        DownloadImage image8= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/girl.png");
        DownloadImage image9= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/monarch.png");
        DownloadImage image10= new DownloadImage("https://homepages.cae.wisc.edu/~ece533/images/peppers.png");

        executor.execute(image1);
        executor.execute(image2);
        executor.execute(image3);
        executor.execute(image4);
        executor.execute(image5);
        executor.execute(image6);
        executor.execute(image7);
        executor.execute(image8);
        executor.execute(image9);
        executor.execute(image10);

        while (true){
            if(executor.getActiveCount()==0){
                System.out.println("\nDownload Completed!");
                executor.shutdown();
                break;
            }
        }
    }
}
